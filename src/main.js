import Vue from 'vue'
import App from './App.vue'
import router from './router'
import http from './http'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import * as echarts from 'echarts'

// 弹框按钮
import {
  Message
} from 'element-ui';
Vue.prototype.$Message = Message

Vue.use(ElementUI);
Vue.config.productionTip = false
Vue.prototype.$http = http
Vue.prototype.$echarts = echarts;




new Vue({
  router,
  render: h => h(App),
}).$mount('#app')