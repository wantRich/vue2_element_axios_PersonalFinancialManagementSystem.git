import Vue from 'vue'
// 导入vue路由
import Router from 'vue-router'
// 以下是导入页面
import login from './views/login/index.vue'
import home from './views/home/index.vue'
import firstpage from './views/firstpage/index.vue'
import pay from './views/pay/index.vue'
import income from './views/income/index.vue'
import category from './views/category/index.vue'
import user from './views/user/index.vue'
import setgoals from './views/setgoals/index.vue'
import records from './views/records/index.vue'

// 全局应用路由
Vue.use(Router)

// 路由实现函数
const router = new Router({
    routes: [{
        // 进入页面 直接跳转登录页面
            path: '/',
            redirect: "login"
        },
        {
            path: '/login',
            component: login
        },
        {
            path: '/home',
            component: home,
            redirect: '/firstpage',
            // 内容子页面
            children: [{
                    path: '/firstpage',
                    component: firstpage,
                },
                {
                    path: "/pay",
                    component: pay,
                },
                {
                    path: "/income",
                    component: income,
                },
                {
                    path: "/category",
                    component: category,
                },
                {
                    path: "/user",
                    component: user,
                },
                {
                    path: "/setgoals",
                    component: setgoals,
                },
                {
                    path: "/records",
                    component: records,
                },

            ]
        },
    ]
})

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

// 为路由对象，添加before 导航守卫
router.beforeEach((to, from, next) => {
    next();
})

export default router