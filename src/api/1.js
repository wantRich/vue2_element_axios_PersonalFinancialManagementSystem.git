// 卡片管理模块
import request from '@/utils/request'

// 获取
export function getRecordList(params) {
  return request({
    url: 'http://106.15.76.183:8081/pf/record/getRecordList',
    method: 'get',
    params
  })
}
// 站点
export function getSite() {
  return request({
    url: '/site/list',
    method: 'get',
  })
}
// 工品品牌/仓库
export function getMatched(params) {
  return request({
    url: 'product/matchedAttributeList',
    method: 'get',
    params
  })
}
// 友方品牌
export function getSiteMatched() {
  return request({
    url: '/product/siteMatchedAttributeList',
    method: 'get',
  })
}
// 数据更新时间
export function getSchedule() {
  return request({
    url: 'product/schedule',
    method: 'get',
  })
}
// 导出平台list
export function getSiteList() {
  return request({
    url: 'product/getSiteList',
    method: 'get',
  })
}
// 导出产品库存接口
export function postExportProductStock(data) {
  return request({
    url: 'http://106.15.76.183:8081/pf/record/getRecordList',
    method: 'POST',
    responseType: "arraybuffer",
    data
  })
}