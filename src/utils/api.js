import axios from "axios"



// axios拦截器


// 接口调取成功
axios.interceptors.response.use(success => {
    // 业务逻辑错误
    if (success.status && success.status == 200) {
        if (success.data == false) {
            this.$message.error({
                message: success.data.message
            });
            return;
        }
        if (success.data.message) {
            this.$message.success({
                message: success.data.message
            });
        }
    }
}, error => {

})