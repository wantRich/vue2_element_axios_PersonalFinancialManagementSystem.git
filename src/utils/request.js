import axios from 'axios'
import {
    Message
} from 'element-ui'

const service = axios.create({
    // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
    baseURL:'http://106.15.76.183:8081/pf',
    timeout: 100000 // request timeout
})

service.interceptors.request.use(
    config => {
        let currentUser = JSON.parse(sessionStorage.getItem("UserInfo"))
        if (currentUser && currentUser.token) {
            config.headers['Authorization'] = 'token ' + currentUser.token;
            // config.headers['content-type'] = 'application/x-www-form-urlencoded';
        }
        return config
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => {
        return response
    },
    error => {
        const res = error.response.data
        const status = error.response.status
        // 接口返回5开头状况处理
        if (status >= 500) {
            Message.error('服务繁忙请稍后再试')
            // 接口返回4开头状况处理
        } else if (status >= 400) {
            // 接口返回401处理
            if (res.status === 401) {
                Message.warning("您的账号登录已失效, 请重新登录")
            } else {
                Message.error("未知错误")
                console.error(error)
            }
        }
        return Promise.resolve(error)
    }
)
export default service