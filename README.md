# financialmanagement


## 项目简介

[vue2+element+axios 个人财务管理系统](https://www.bilibili.com/video/BV1894y1L7wX/?spm_id_from=333.999.0.0&vd_source=ca6e73a686e8d4c2a353baca46aee851) 是基于 vue2+element+axios 等主流技术栈构建的免费开源的后台管理前端模板。


## 项目特色

- **简洁易用**：无过渡封装 ，易上手。


- **权限管理**：用户、角色、菜单、字典、部门等完善的权限系统功能。

- **基础设施**：路由、代码规范、Git 提交规范、常用组件封装。

- **持续更新**：持续更新，及时跟进最新的技术和工具。 



## 项目预览
登录页面的为动态效果，鼠标移动或者悬浮都有不同的动态效果


![登录页面](https://gitee.com/naitang_room/images/raw/master/9E9A86E0F838B1E2EDA83123B7BE4BAD.png)

![登录页面](https://gitee.com/naitang_room/images/raw/master/6EB4A7E4CCFE454B585B42CE5B02355F.png)


![登录页面](https://gitee.com/naitang_room/images/raw/master/A9815FBC14FD168BA0D02BF67A187C02.png)



![登录页面](https://gitee.com/naitang_room/images/raw/master/4DD79423E2C3500F882AB502D3F80E28.png)


![支出页面](https://gitee.com/naitang_room/images/raw/master/EAFC8076865E88D2175E94987DBAD5BC.png)
![收入页面](https://gitee.com/naitang_room/images/raw/master/E508DBEF822546C20B826C522E0DEB5A.png)



![首页](https://gitee.com/naitang_room/images/raw/master/473E4DB4D256BD06B48151B126E4FFEB.png)

![设定目标](https://gitee.com/naitang_room/images/raw/master/30C91EE217235EC27B437F7C19B878F3.png)



## 环境准备

| 环境                 | 名称版本                                                     | 下载地址                                                     |
| -------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **开发工具**         | VSCode                                                       | [下载](https://code.visualstudio.com/Download)           |
| **运行环境**         | Node ≥18                                                    | [下载](http://nodejs.cn/download)                        |


## 项目启动

```bash
# 克隆代码
git clone  https://gitee.com/naitang_room/Personal-Financial-Management-System.git


# 安装依赖
npm install

# 启动运行
npm run serve

# 打包
npm run build


```



## 项目部署

```bash
# 项目打包
npm run build:prod

```







## 交流群🚀

> **关注「农民工前端」公众号，获取交流群二维码。**
>
> 如果交流群的二维码过期，请加微信(nmgcd_)并备注「前端开发进群」以获取最新二维码。
>
> 为确保交流群质量，防止营销广告人群混入，我们采取了此措施。望各位理解！



